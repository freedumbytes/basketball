package nl.demon.shadowland.freedumbytes.sports.basketball.rest.server.health;


import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * Ah, I see you have the machine that goes 'ping!'.
 *
 * This is my favourite. You see, we lease this back from the company we sold it to - that way it comes under the monthly current budget and not the capital account.
 *
 * — The Meaning of Life
 */
@Path("/ping")
@Tag(name = "health")
@Singleton
@Component
public class PingResource
{
  @Value("${application.name}")
  private String applicationName;

  @Value("${application.company}")
  private String company;

  @Value("${application.version}")
  private String version;

  @Value("${application.build}")
  private String build;


  /**
   * Health Check.
   *
   * @return pong.
   */
  @GET
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
//@formatter:off
  @Operation(
      summary = "Application health.",
      description = "Check application availability.",
      responses = {
          @ApiResponse(
              responseCode = "200",
              description = "Application information when it is up and running.",
              content = {
                  @Content(
                      mediaType = MediaType.APPLICATION_JSON,
                      schema = @Schema(
                          implementation = ApplicationInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XML,
                      schema = @Schema(
                          implementation = ApplicationInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XHTML_XML,
                      schema = @Schema(
                          implementation = ApplicationInfo.class
                          )
                      )
                  }
              ),
          @ApiResponse(
              responseCode = "404",
              description = "Application is down."
              )
          }
      )
//@formatter:on
  public ApplicationInfo pong()
  {
    return ApplicationInfo.of(applicationName, company, version, build);
  }
}
