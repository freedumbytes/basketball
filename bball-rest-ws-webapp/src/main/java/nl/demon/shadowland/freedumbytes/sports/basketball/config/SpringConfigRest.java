package nl.demon.shadowland.freedumbytes.sports.basketball.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.extern.slf4j.Slf4j;


@Configuration
@ComponentScan(basePackages = { "nl.demon.shadowland.freedumbytes.sports.basketball" })
@PropertySource(value = "classpath:bball/application.properties")
@Slf4j
public class SpringConfigRest
{
  public SpringConfigRest()
  {
    log.info("Run Spring REST wiring.");
  }
}
