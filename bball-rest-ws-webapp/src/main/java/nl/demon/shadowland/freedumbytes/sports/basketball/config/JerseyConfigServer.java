package nl.demon.shadowland.freedumbytes.sports.basketball.config;


import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;


@Component
@ApplicationPath("/api")
@Slf4j
// @formatter:off
@OpenAPIDefinition(
    info = @Info(
        title = "Basketball REST",
        description = "# Basketball Games & Stats API\n"
            + "Score Sheet and Game Statistics made available with the following frameworks:\n"
            + "* [Jersey](https://jersey.github.io/) RESTful Web Services in Java.\n"
            + "* [Swagger Core](https://github.com/swagger-api/swagger-core) is a Java implementation of the OpenAPI Specification.\n"
            + "* [Swagger UI](https://github.com/swagger-api/swagger-ui) to visualize and interact with the API's resources without having any of the implementation logic in place.\n"
            + "* [The OpenAPI Specification](https://swagger.io/specification/) or [OAS](https://github.com/OAI/OpenAPI-Specification/) defines a standard, language-agnostic interface to RESTful APIs.\n"
            + "* [Swagger Editor](https://github.com/swagger-api/swagger-editor/) lets you [edit](https://editor.swagger.io/) Swagger API specifications in YAML inside your browser and to preview documentations in real time.\n"
            + "* [Maven Plugin](https://github.com/swagger-api/swagger-codegen/blob/master/modules/swagger-codegen-maven-plugin/README.md) to support the [Swagger Codegen project](https://github.com/swagger-api/swagger-codegen/).\n"
            + "* [Tools and Integrations](https://swagger.io/tools/open-source/open-source-integrations/) a list of libraries and frameworks serving the Swagger ecosystem.\n"
            + "* [Single Source of Truth](https://idratherbewriting.com/learnapidoc/pubapis_combine_swagger_and_guide.html): Integrating Swagger UI with the rest of your docs.\n",
        contact = @Contact(
            name = "Free Dumb Bytes aka freedumbytes",
            url = "https://gitlab.com/freedumbytes/basketball/",
            email = "freedumbytes@shadowland.demon.nl"
            ),
        license = @License(
            name = "Apache License, Version 2.0.",
            url = "https://www.apache.org/licenses/LICENSE-2.0.html"
            ),
        version = "1.0.0"
        ),
    servers = {
        @Server(
            url = "http://freedumbytes.dev.net/tomcat/bball/",
            description = "LAN"
            ),
        @Server(
            url = "http://www.shadowland.demon.nl/basketball.xhtml",
            description = "WWW"
            )
        },
    tags = {
        @Tag(
            name = "health",
            description = "Application availability."
            )
        },
    externalDocs = @ExternalDocumentation(
        description = "OpenAPI 3.0 tutorial.",
        url = "https://idratherbewriting.com/learnapidoc/pubapis_openapi_tutorial_overview.html"
        )
    )
 // @formatter:on
public class JerseyConfigServer extends ResourceConfig
{
  public JerseyConfigServer()
  {
    log.info("Run Jersey REST wiring.");

    packages("nl.demon.shadowland.freedumbytes.sports.basketball.rest", "nl.demon.shadowland.freedumbytes.rest.param.provider", "io.swagger.v3.jaxrs2.integration.resources");

    JerseyConfigBuilder builder = new JerseyConfigBuilder();

    builder.enableMoxyXmlFeature();
    builder.enableMoxyJsonIncludeRoot();
    builder.enableMoxyJsonFormattedOutput();
    builder.enableBeanValidationSendErrorInResponse();
    builder.enableLogTrafficDumpEntityLoggingFeatureAlternative();

    builder.configureServer(this);
  }
}
