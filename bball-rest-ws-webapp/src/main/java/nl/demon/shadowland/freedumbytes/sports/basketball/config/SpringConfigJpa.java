package nl.demon.shadowland.freedumbytes.sports.basketball.config;


import java.util.Properties;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import lombok.extern.slf4j.Slf4j;


@Configuration
@EnableTransactionManagement
@Slf4j
public class SpringConfigJpa
{
  private static final String JNDI_JDBC_BBALL = "java:comp/env/jdbc/BBall";

  @Value("${hibernate.dialect}")
  private String hibernateDialect;

  @Value("${hibernate.hbm2ddl.auto:}")
  private String hibernateHbm2ddlAuto;

  @Value("${hibernate.show_sql:false}")
  private String hibernateShowSql;

  @Value("${hibernate.format_sql:false}")
  private String hibernateFormatSql;

  @Value("${hibernate.bytecode.provider:javassist}")
  private String hibernateBytecodeProvider;

  @Value("${hibernate.cache.use_query_cache:false}")
  private String hibernateCacheUseQueryCache;

  @Value("${hibernate.cache.use_second_level_cache:false}")
  private String hibernateCacheUseSecondLevelCache;


  public SpringConfigJpa()
  {
    log.info("Run Spring JPA wiring.");
  }


  @Bean
  public PlatformTransactionManager transactionManager() throws NamingException
  {
    return new JpaTransactionManager(entityManagerFactory());
  }


  @Bean
  public EntityManagerFactory entityManagerFactory() throws NamingException
  {
    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setPersistenceUnitName("emf-bball");
    factory.setJpaVendorAdapter(jpaVendorAdapter);

    DataSource dataSource = dataSource();
    factory.setDataSource(dataSource);

    factory.setPackagesToScan(new String[] { "nl.demon.shadowland.freedumbytes.sports.basketball.competition.domain" });
    factory.setJpaProperties(jpaProperties());

    factory.afterPropertiesSet();

    return factory.getObject();
  }


  @Bean
  public DataSource dataSource() throws NamingException
  {
    return (DataSource) new JndiTemplate().lookup(JNDI_JDBC_BBALL);
  }


  protected Properties jpaProperties()
  {
    Properties jpaProperties = new Properties();

    jpaProperties.put(Environment.DIALECT, hibernateDialect);
    jpaProperties.put(Environment.HBM2DDL_AUTO, hibernateHbm2ddlAuto);
    jpaProperties.put(Environment.SHOW_SQL, hibernateShowSql);
    jpaProperties.put(Environment.FORMAT_SQL, hibernateFormatSql);
    jpaProperties.put(Environment.BYTECODE_PROVIDER, hibernateBytecodeProvider);
    jpaProperties.put(Environment.USE_QUERY_CACHE, hibernateCacheUseQueryCache);
    jpaProperties.put(Environment.USE_SECOND_LEVEL_CACHE, hibernateCacheUseSecondLevelCache);

    return jpaProperties;
  }
}
