package nl.demon.shadowland.freedumbytes.sports.basketball.servlet.spring;


import java.util.Arrays;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import nl.demon.shadowland.freedumbytes.sports.basketball.config.SpringConfigRest;

import lombok.extern.slf4j.Slf4j;


@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class WebContainerInitializer implements WebApplicationInitializer
{
  @Override
  public void onStartup(ServletContext servletContext) throws ServletException
  {
    log.info("Run WebApp REST wiring.");

    registerContextLoaderListener(servletContext);

    disableDefaultJerseySpringIntegration(servletContext);
  }


  private void registerContextLoaderListener(ServletContext servletContext)
  {
    WebApplicationContext webContext = createWebAplicationContext(SpringConfigRest.class);

    servletContext.addListener(createContextLoaderListener(webContext));
  }


  private WebApplicationContext createWebAplicationContext(Class<?>... configClasses)
  {
    Arrays.stream(configClasses).forEach(configClass -> log.info("configClass: {}", configClass));

    AnnotationConfigWebApplicationContext context = createAnnotationConfigWebApplicationContext();

    context.register(configClasses);

    return context;
  }


  AnnotationConfigWebApplicationContext createAnnotationConfigWebApplicationContext()
  {
    return new AnnotationConfigWebApplicationContext();
  }


  ContextLoaderListener createContextLoaderListener(WebApplicationContext webContext)
  {
    return new ContextLoaderListener(webContext);
  }


  /**
   * Set the Jersey used property so it won't load a ContextLoaderListener.
   *
   * @param servletContext
   */
  private void disableDefaultJerseySpringIntegration(ServletContext servletContext)
  {
    servletContext.setInitParameter("contextConfigLocation", "nl.demon.shadowland.freedumbytes.sports.basketball");
  }
}
