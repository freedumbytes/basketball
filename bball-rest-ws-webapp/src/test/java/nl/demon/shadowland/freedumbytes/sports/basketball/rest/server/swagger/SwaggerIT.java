package nl.demon.shadowland.freedumbytes.sports.basketball.rest.server.swagger;


import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import nl.demon.shadowland.freedumbytes.rest.test.JerseySpringIT;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.JerseyConfigServer;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.SpringConfigRest;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.TestSpringConfigJpa;


@ContextConfiguration(classes = { SpringConfigRest.class, TestSpringConfigJpa.class })
public class SwaggerIT extends JerseySpringIT
{
  public final static MediaType APPLICATION_YAML_TYPE = new MediaType("application", "yaml");

  @Value("${application.version}")
  private String version;

  @Value("${application.build}")
  private String build;

  @Autowired
  private JerseyConfigServer jerseyConfigServer;


  @Test
  public void openapiYAMLAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    String openapiYaml = target.request(APPLICATION_YAML_TYPE).get(String.class);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
  }


  @Test
  public void openapiJSONAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    String openapiJson = target.request(MediaType.APPLICATION_JSON_TYPE).get(String.class);

    assertThat(openapiJson).contains("\"openapi\"");
  }


  @Test
  public void openapiXMLAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    Response response = target.request(MediaType.APPLICATION_XML_TYPE).get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
  }


  @Test
  public void openapiXHTMLAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    Response response = target.request(MediaType.APPLICATION_XHTML_XML_TYPE).get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
  }


  @Test
  public void openapiYAMLOpenApiResource()
  {
    WebTarget target = target("openapi.yaml");
    String openapiYaml = target.request().get(String.class);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
  }


  @Test
  public void openapiJSONOpenApiResource()
  {
    WebTarget target = target("openapi.json");
    String openapiJson = target.request().get(String.class);

    assertThat(openapiJson).contains("\"openapi\"");
  }


  @Test
  public void openapiXMLOpenApiResource()
  {
    WebTarget target = target("openapi.xml");
    Response response = target.request().get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_FOUND);
  }


  @Test
  public void openapiXHTMLOpenApiResource()
  {
    WebTarget target = target("openapi.xhtml");
    Response response = target.request().get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_FOUND);
  }


  @Override
  protected ResourceConfig getResourceConfigServer()
  {
    return jerseyConfigServer;
  }
}
