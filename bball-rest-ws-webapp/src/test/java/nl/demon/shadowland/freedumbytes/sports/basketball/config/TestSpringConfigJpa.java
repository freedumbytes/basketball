package nl.demon.shadowland.freedumbytes.sports.basketball.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


@PropertySources({ @PropertySource(value = "classpath:bball/h2-jdbc.properties", ignoreResourceNotFound = false), @PropertySource(value = "classpath:bball/h2-hibernate.properties", ignoreResourceNotFound = true) })
public class TestSpringConfigJpa
{
  @Value("${jdbc.driverClassName}")
  private String jdbcDriverClassName;

  @Value("${jdbc.url}")
  private String jdbcUrl;

  @Value("${jdbc.username}")
  private String jdbcUsername;

  @Value("${jdbc.password}")
  private String jdbcPassword;


  @Bean
  public DataSource dataSource()
  {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName(jdbcDriverClassName);
    dataSource.setUrl(jdbcUrl);
    dataSource.setUsername(jdbcUsername);
    dataSource.setPassword(jdbcPassword);

    return dataSource;
  }
}
