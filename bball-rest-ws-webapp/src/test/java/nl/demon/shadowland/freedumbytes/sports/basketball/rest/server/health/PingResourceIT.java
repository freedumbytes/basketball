package nl.demon.shadowland.freedumbytes.sports.basketball.rest.server.health;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import nl.demon.shadowland.freedumbytes.rest.test.JerseySpringIT;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.JerseyConfigServer;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.SpringConfigRest;
import nl.demon.shadowland.freedumbytes.sports.basketball.config.TestSpringConfigJpa;


@ContextConfiguration(classes = { SpringConfigRest.class, TestSpringConfigJpa.class })
public class PingResourceIT extends JerseySpringIT
{
  @Value("${application.version}")
  private String version;

  @Value("${application.build}")
  private String build;

  @Autowired
  private JerseyConfigServer jerseyConfigServer;


  @Test
  public void pingJSON()
  {
    final WebTarget target = target("ping");
    final ApplicationInfo applicationInfo = target.request(MediaType.APPLICATION_JSON_TYPE).get(ApplicationInfo.class);

    assertApplicationInfo(applicationInfo);
  }


  @Test
  public void pingXML()
  {
    final WebTarget target = target("ping");
    final ApplicationInfo applicationInfo = target.request(MediaType.APPLICATION_XML_TYPE).get(ApplicationInfo.class);

    assertApplicationInfo(applicationInfo);
  }


  @Test
  public void pingXHTML()
  {
    final WebTarget target = target("ping");
    final ApplicationInfo applicationInfo = target.request(MediaType.APPLICATION_XHTML_XML_TYPE).get(ApplicationInfo.class);

    assertApplicationInfo(applicationInfo);
  }


  /**
   * Note: In case of 'java.lang.AssertionError: Expecting: <"${project.build.timestamp}"> to end with: <"CET" or "CEST">'.
   *
   * Eclipse copied the application.properties to the target folder and m2e replaced the standard Maven ${property} such as ${parent.name}, ${parent.organization.name} and ${project.version}.
   *
   * But ${project.build.timestamp} is defined with the build-helper-maven-plugin plugin in the parent POM during the validate phase.
   *
   * Just run 'mvn process-resources' to fix this.
   *
   * @param applicationInfo
   */
  private void assertApplicationInfo(final ApplicationInfo applicationInfo)
  {
    assertThat(applicationInfo.getName()).isEqualTo("Basketball Games & Stats");
    assertThat(applicationInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(applicationInfo.getVersion()).isEqualTo(version);
    assertThat(applicationInfo.getBuild()).isEqualTo(build).containsPattern(".CET|.CEST");
    assertThat(applicationInfo.getTime()).isNotNull();
  }


  @Test
  public void daylightSavingTime()
  {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");

    ZonedDateTime summerDateTime = ZonedDateTime.of(2018, 10, 28, 2, 59, 59, 0, ZoneId.of("Europe/Amsterdam"));
    assertThat(summerDateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME)).isEqualTo("Sun, 28 Oct 2018 02:59:59 +0200");
    assertThat(summerDateTime.format(formatter)).isEqualTo("2018-10-28 02:59:59 CEST");

    ZonedDateTime winterDateTime = ZonedDateTime.of(2018, 10, 28, 3, 0, 0, 0, ZoneId.of("Europe/Amsterdam"));
    assertThat(winterDateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME)).isEqualTo("Sun, 28 Oct 2018 03:00:00 +0100");
    assertThat(winterDateTime.format(formatter)).isEqualTo("2018-10-28 03:00:00 CET");
  }


  @Test
  public void jaxbContext() throws JAXBException
  {
    assertThat(JAXBContext.newInstance(PingResource.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
    assertThat(JAXBContext.newInstance(ApplicationInfo.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
    assertThat(JAXBContext.newInstance(ApplicationInfo.class, PingResource.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
    assertThat(JAXBContext.newInstance(PingResource.class, ApplicationInfo.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
  }


  @Override
  protected ResourceConfig getResourceConfigServer()
  {
    return jerseyConfigServer;
  }
}
