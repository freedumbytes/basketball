package nl.demon.shadowland.freedumbytes.sports.basketball.rest.server.health;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.Test;


public class ApplicationInfoTest
{
  private static final String APP_NAME = "Basketball";
  private static final String APP_COMPANY = "freedumbytes";
  private static final String APP_VERSION = "1.0.0";
  private static final String APP_BUILD = "#1";
  private static final LocalDateTime APP_TIME = LocalDateTime.now();


  @Test
  public void setters()
  {
    ApplicationInfo applicationInfo = new ApplicationInfo();

    assertThat(APP_TIME).isBefore(applicationInfo.getTime());

    applicationInfo.setName(APP_NAME);
    applicationInfo.setCompany(APP_COMPANY);
    applicationInfo.setVersion(APP_VERSION);
    applicationInfo.setBuild(APP_BUILD);
    applicationInfo.setTime(APP_TIME);

    assertApplicationInfo(applicationInfo);
  }


  @Test(expected = NullPointerException.class)
  public void setNameRequired()
  {
    ApplicationInfo applicationInfo = new ApplicationInfo();

    applicationInfo.setName(null);
  }


  @Test(expected = NullPointerException.class)
  public void setCompanyRequired()
  {
    ApplicationInfo applicationInfo = new ApplicationInfo();

    applicationInfo.setCompany(null);
  }


  @Test(expected = NullPointerException.class)
  public void setVersionRequired()
  {
    ApplicationInfo applicationInfo = new ApplicationInfo();

    applicationInfo.setVersion(null);
  }


  @Test(expected = NullPointerException.class)
  public void setBuildRequired()
  {
    ApplicationInfo applicationInfo = new ApplicationInfo();

    applicationInfo.setBuild(null);
  }


  @Test
  public void of()
  {
    ApplicationInfo applicationInfo = ApplicationInfo.of(APP_NAME, APP_COMPANY, APP_VERSION, APP_BUILD);

    assertThat(APP_TIME).isBefore(applicationInfo.getTime());

    applicationInfo.setTime(APP_TIME);

    assertApplicationInfo(applicationInfo);
  }


  @Test(expected = NullPointerException.class)
  public void ofNameRequired()
  {
    ApplicationInfo.of(null, APP_COMPANY, APP_VERSION, APP_BUILD);
  }


  @Test(expected = NullPointerException.class)
  public void ofCompanyRequired()
  {
    ApplicationInfo.of(APP_NAME, null, APP_VERSION, APP_BUILD);
  }


  @Test(expected = NullPointerException.class)
  public void ofVersionRequired()
  {
    ApplicationInfo.of(APP_NAME, APP_COMPANY, null, APP_BUILD);
  }


  @Test(expected = NullPointerException.class)
  public void ofBuildRequired()
  {
    ApplicationInfo.of(APP_NAME, APP_COMPANY, APP_VERSION, null);
  }


  @Test
  public void twoString()
  {
    ApplicationInfo applicationInfo = ApplicationInfo.of(APP_NAME, APP_COMPANY, APP_VERSION, APP_BUILD);

    applicationInfo.setTime(LocalDateTime.of(2018, 11, 18, 23, 58, 59, 123456789));

    System.out.println(applicationInfo.toString());
    assertThat(applicationInfo.toString()).isEqualTo("ApplicationInfo(name=Basketball, company=freedumbytes, version=1.0.0, build=#1, time=2018-11-18T23:58:59.123456789)");
  }


  private void assertApplicationInfo(final ApplicationInfo applicationInfo)
  {
    assertThat(applicationInfo.getName()).isEqualTo(APP_NAME);
    assertThat(applicationInfo.getCompany()).isEqualTo(APP_COMPANY);
    assertThat(applicationInfo.getVersion()).isEqualTo(APP_VERSION);
    assertThat(applicationInfo.getBuild()).isEqualTo(APP_BUILD);
    assertThat(applicationInfo.getTime()).isEqualTo(APP_TIME);
  }
}
