package nl.demon.shadowland.freedumbytes.sports.basketball.servlet.spring;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import nl.demon.shadowland.freedumbytes.sports.basketball.config.SpringConfigRest;
import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class WebContainerInitializerTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(WebContainerInitializer.class);

  @Mock
  private ServletContext servletContext;

  @Mock
  private AnnotationConfigWebApplicationContext annotationConfigWebApplicationContext;

  @Mock
  private ContextLoaderListener contextLoaderListener;

  @Spy
  private WebContainerInitializer webContainerInitializer = new WebContainerInitializer();


  @Test
  public void doFilterServletRequestWithSpy() throws IOException, ServletException
  {
    doReturn(annotationConfigWebApplicationContext).when(webContainerInitializer).createAnnotationConfigWebApplicationContext();
    doReturn(contextLoaderListener).when(webContainerInitializer).createContextLoaderListener(annotationConfigWebApplicationContext);

    webContainerInitializer.onStartup(servletContext);

    verify(annotationConfigWebApplicationContext).register(SpringConfigRest.class);
    verify(servletContext).addListener(contextLoaderListener);

    verifyWebContainerInitializer();
    verifyLogging();
  }


  @Test
  public void doFilterServletRequestWithoutSpy() throws IOException, ServletException
  {
    webContainerInitializer.onStartup(servletContext);

    verify(servletContext).addListener(any(ContextLoaderListener.class));

    verifyWebContainerInitializer();
    verifyLogging();
  }


  private void verifyWebContainerInitializer()
  {
    verify(servletContext).setInitParameter("contextConfigLocation", "nl.demon.shadowland.freedumbytes.sports.basketball");
  }


  private void verifyLogging()
  {
    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(0).getMessage()).isEqualTo("Run WebApp REST wiring.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(1).getMessage()).isEqualTo("configClass: class nl.demon.shadowland.freedumbytes.sports.basketball.config.SpringConfigRest");
  }
}
