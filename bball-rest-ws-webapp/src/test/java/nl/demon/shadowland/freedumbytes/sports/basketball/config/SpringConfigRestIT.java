package nl.demon.shadowland.freedumbytes.sports.basketball.config;


import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringConfigRest.class, TestSpringConfigJpa.class })
public class SpringConfigRestIT
{
  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private JerseyConfigServer jerseyConfigServer;

  @Autowired
  private PlatformTransactionManager transactionManager;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  @Autowired
  private DataSource dataSource;


  @Test
  public void springWiring()
  {
    assertThat(applicationContext).isNotNull();
    assertThat(jerseyConfigServer).isNotNull();

    assertThat(transactionManager).isNotNull();
    assertThat(entityManagerFactory).isNotNull();
    assertThat(dataSource).isNotNull();
  }
}
