# Basketball

[![Basketball License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Basketball License")](https://www.apache.org/licenses/LICENSE-2.0.html)
[![Basketball pipeline](https://gitlab.com/freedumbytes/basketball/badges/master/build.svg "Basketball pipeline")](https://gitlab.com/freedumbytes/basketball)

[![Basketball Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Basketball Maven Site")](https://freedumbytes.gitlab.io/basketball/)
[![Basketball Maven Site](https://javadoc.io/badge/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?color=yellow&label=Basketball "Basketball Maven Site")](https://freedumbytes.gitlab.io/basketball/)

[![Basketball Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Basketball Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.sports.basketball%22)
[![Basketball Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?label=Maven%20Central "Basketball Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.sports.basketball%22)

[![Basketball Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Basketball Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.sports.basketball~~~~)
[![Basketball Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?label=Nexus "Basketball Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.sports.basketball~~~~)

[![Basketball MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Basketball MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.sports.basketball)
[![Basketball MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?label=MvnRepository "Basketball MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.sports.basketball)

[![Basketball SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Basketball SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs)
[![Basketball Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=alert_status "Basketball Quality Gate")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs)
[![Basketball vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=vulnerabilities "Basketball vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=vulnerabilities)
[![Basketball bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=bugs "Basketball bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=bugs)
[![Basketball coverage](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=coverage "Basketball coverage")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=coverage)

[![Basketball SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Basketball SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs)
[![Basketball lines of code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=ncloc "Basketball lines of code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=ncloc)
[![Basketball duplication](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=duplicated_lines_density "Basketball duplication")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=duplicated_lines)
[![Basketball technical debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=sqale_index "Basketball technical debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.sports.basketball:bball-docs&metric=sqale_index)

[![Basketball Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Basketball Dependency Check")](https://freedumbytes.gitlab.io/basketball/dependency-check-report.html)
[![Basketball Dependency Check](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?label=Dependency%20Check "Basketball Dependency Check")](https://freedumbytes.gitlab.io/basketball/dependency-check-report.html)

[![Basketball Javadoc.io](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Basketball Javadoc.io")](https://javadoc.io/doc/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs)
[![Basketball Javadoc.io](https://javadoc.io/badge/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs.svg?color=yellow&label=Javadoc "Basketball Javadoc.io")](https://javadoc.io/doc/nl.demon.shadowland.freedumbytes.sports.basketball/bball-docs)

#### History

In early December 1891, Canadian [Dr. James Naismith](https://en.wikipedia.org/wiki/James_Naismith), a physical education professor
and instructor at the International Young Men's Christian Association Training School (YMCA) (today, Springfield College) in Springfield,
Massachusetts, (USA), was trying to keep his gym class active on a rainy day. He sought a vigorous indoor game to keep his students occupied
and at proper levels of fitness during the long New England winters.  After rejecting other ideas as either too rough or poorly suited
to walled-in gymnasiums, he wrote the basic rules and nailed a peach basket onto a 10-foot (3.048 m) elevated track. In contrast
with modern basketball nets, this peach basket retained its bottom, and balls had to be retrieved manually after each basket or point scored;
this proved inefficient, however, so the bottom of the basket was removed, allowing the balls to be poked out with a long dowel each time.

Naismith's handwritten diaries, discovered by his granddaughter in early 2006, indicate that he was nervous about the new game he had invented,
which incorporated rules from a children's game called *Duck on a Rock*, as many had failed before it. Naismith called the new game *Basket Ball*.

| ![Dr. James Naismith, Inventor of Basketball](https://freedumbytes.gitlab.io/basketball/images/james-naismith-statue-almonte.jpg "Dr. James Naismith, Inventor of Basketball") |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Sculpture *Dr. James Naismith, Inventor of Basketball*, Almonte, Ontario, (Canada). (Robertson, 2012).                                                                                 |

Basketball was originally played with a soccer ball. The first balls made specifically for basketball were brown,
and it was only in the late 1950s that Tony Hinkle, searching for a ball that would be more visible to players and spectators alike,
introduced the orange ball that is now in common use. Dribbling was not part of the original game except for the bounce pass to teammates.
Passing the ball was the primary means of ball movement. Dribbling was eventually introduced but limited by the asymmetric shape of early balls.
Dribbling only became a major part of the game around the 1950s, as manufacturing improved the ball shape.

| ![The first basketball court at Springfield College](https://freedumbytes.gitlab.io/basketball/images/first-basketball-court-springfield.jpg "The first basketball court at Springfield College") |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| The first basketball court at Springfield College in Springfield, Massachusetts, USA. Date 1891. (Hiskey, 2012).                                                                       |

The peach baskets were used until 1906 when they were finally replaced by metal hoops with backboards. A further change was soon made,
so the ball merely passed through. Whenever a person got the ball in the basket, his team would gain a point.
Whichever team got the most points won the game. The baskets were originally nailed to the mezzanine balcony of the playing court,
but this proved impractical when spectators on the balcony began to interfere with shots.
The backboard was introduced to prevent this interference; it had the additional effect of allowing rebound shots.

The first official game was played in the YMCA gymnasium in Albany, New York on January 20, 1892 with nine players. The game ended at 1-0;
the shot was made from 25 feet (7.6 m), on a court just half the size of a present-day Streetball or National Basketball Association (NBA) court.
By 1897-1898 teams of five became standard.

#### Original rules

James Naismith devised a set of thirteen rules of basketball:

 1. The ball may be thrown in any direction with one or both hands.

 2. The ball may be batted in any direction with one or both hands, but never with the fist.

 3. A player cannot run with the ball. The player must throw it from the spot on which he catches it,
      allowance to be made for a man running at good speed.

 4. The ball must be held in or between the hands. The arms or body must not be used for holding it.

 5. No shouldering, holding, pushing, striking or tripping in any way of an opponent.
      The first infringement of this rule by any person shall count as a foul;
      the second shall disqualify him until the next goal is made or, if there was evident intent to injure the person, for the whole of the game.
      No substitution shall be allowed.

 6. A foul is striking at the ball with the fist, violations of Rules 3 and 4 and such as described in Rule 5.

 7. If either side make three consecutive fouls it shall count as a goal for the opponents
      (consecutive means without the opponents in the meantime making a foul).

 8. A goal shall be made when the ball is thrown or batted from the ground into the basket and stays there,
      providing those defending the goal do not touch or disturb the goal.
      If the ball rests on the edge and the opponents move the basket, it shall count as a goal.

 9. When the ball goes out of bounds, it shall be thrown into the field and played by the first person touching it.
      In case of dispute the umpire shall throw it straight into the field. The thrower-in is allowed five seconds.
      If he holds it longer, it shall go to the opponent. If any side persists in delaying the game, the umpire shall call a foul on them.

 10. The umpire shall be the judge of the men and shall note the fouls and notify the referee when three consecutive fouls have been made.
      He shall have the power to disqualify men according to Rule 5.

 11. The referee shall be the judge of the ball and decide when it is in play in bounds, to which side it belongs, and shall keep the time.
      He shall decide when a goal has been made and keep account of the goals with any other duties that are usually performed by a referee.

 12. The time shall be two 15-minute halves with five minutes rest between.

 13. The side making the most goals in that time shall be declared the winners.

# Bibliography

 * Wikipedia contributors. (2001). *Basketball*. [online]
   Available at: [https://en.wikipedia.org/wiki/Basketball](https://en.wikipedia.org/wiki/Basketball)
   [Accessed October 24, 2018].

 * Wikipedia contributors. (2008). *History of basketball*. [online]
   Available at: [https://en.wikipedia.org/wiki/History_of_basketball](https://en.wikipedia.org/wiki/History_of_basketball)
   [Accessed October 24, 2018].

 * Wikipedia contributors. (2005). *Rules of basketball*. [online]
   Available at: [https://en.wikipedia.org/wiki/Rules_of_basketball](https://en.wikipedia.org/wiki/Rules_of_basketball)
   [Accessed October 24, 2018].

 * Hiskey, Daven. (2012). *The Origin of Basketball*. [online]
   Available at: [https://www.todayifoundout.com/index.php/2012/01/the-origin-of-basketball](https://www.todayifoundout.com/index.php/2012/01/the-origin-of-basketball)
   [Accessed October 24, 2018].

# Reference

 * Robertson, D. Gordon E. (2012). Sculpture *Dr. James Naismith, Inventor of Basketball*, Almonte, Ontario, (Canada), sculptor: Elden Tefft, Lawrence, Kansas, dedicated 23 July 2011. [online]
   Available at: [https://en.wikipedia.org/wiki/James_Naismith](https://en.wikipedia.org/wiki/James_Naismith)
   [Accessed October 24, 2018].

# Java Frameworks

Score Sheet and Game Statistics made available with the following frameworks:

## Jersey

 * [Jersey](https://jersey.github.io/) RESTful Web Services in Java.

Jersey contains support for [Web Application Description Language](https://en.wikipedia.org/wiki/Web_Application_Description_Language) (WADL).
WADL is a XML description of a deployed RESTful web application. It contains model of the deployed resources, their structure, supported media types, HTTP methods and so on.
In a sense, WADL is a similar to the WSDL ([Web Services Description Language](https://en.wikipedia.org/wiki/Web_Services_Description_Language)) which describes SOAP web services.
WADL is however specifically designed to describe RESTful Web resources.

For example `./bball/api/application.wadl` or `./bball/api/application.wadl?detail=true`:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<application xmlns="http://wadl.dev.java.net/2009/02">
  <doc xmlns:jersey="http://jersey.java.net/" jersey:generatedBy="Jersey: 2.28 2019-01-25 15:18:13"/>
  <doc xmlns:jersey="http://jersey.java.net/" jersey:hint="This is simplified WADL with user and core resources only. To get full WADL with extended resources use the query parameter detail. Link: ./bball/api/application.wadl?detail=true"/>
  <grammars>
    <include href="./bball/api/application.wadl/xsd0.xsd">
      <doc title="Generated" xml:lang="en"/>
    </include>
  </grammars>
  <resources base="http://freedumbytes.dev.net/bball/api/">
    <resource path="/ping">
      <method id="pong" name="GET">
        <response>
          <ns2:representation xmlns:ns2="http://wadl.dev.java.net/2009/02" xmlns="" element="appInfo" mediaType="application/json"/>
          <ns2:representation xmlns:ns2="http://wadl.dev.java.net/2009/02" xmlns="" element="appInfo" mediaType="application/xml"/>
          <ns2:representation xmlns:ns2="http://wadl.dev.java.net/2009/02" xmlns="" element="appInfo" mediaType="application/xhtml+xml"/>
        </response>
      </method>
    </resource>
    <resource path="/openapi.{type:json|yaml}">
      <param xmlns:xs="http://www.w3.org/2001/XMLSchema" name="type" style="template" type="xs:string"/>
      <method id="getOpenApi" name="GET">
        <response>
          <representation mediaType="application/json"/>
          <representation mediaType="application/yaml"/>
        </response>
      </method>
    </resource>
    <resource path="/openapi">
      <method id="getOpenApiYaml" name="GET">
        <response>
          <representation mediaType="application/yaml"/>
        </response>
      </method>
      <method id="getOpenApiJson" name="GET">
        <response>
          <representation mediaType="application/json"/>
        </response>
      </method>
    </resource>
  </resources>
</application>
```

and included `./bball/api/application.wadl/xsd0.xsd`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:complexType name="applicationInfo">
    <xsd:sequence>
      <xsd:element name="name" type="xsd:string" minOccurs="0"/>
      <xsd:element name="company" type="xsd:string" minOccurs="0"/>
      <xsd:element name="version" type="xsd:string" minOccurs="0"/>
      <xsd:element name="build" type="xsd:string" minOccurs="0"/>
      <xsd:element name="time" type="xsd:string" minOccurs="0"/>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name="appInfo" type="applicationInfo"/>
</xsd:schema>
```

## OpenAPI Specification

 * [The OpenAPI Specification](https://swagger.io/specification/) or [OAS](https://github.com/OAI/OpenAPI-Specification/) defines a standard, language-agnostic interface to RESTful APIs.
 * [OpenAPI 3.0 tutorial](https://idratherbewriting.com/learnapidoc/pubapis_openapi_tutorial_overview.html).

The OpenAPI Specification, originally known as the Swagger Specification, is a specification for machine-readable interface files for describing, producing, consuming, and visualizing RESTful web services.
Originally part of the Swagger framework, it became a separate project in 2016, overseen by the OpenAPI Initiative, an open source collaborative project of the Linux Foundation.
Swagger and some other tools can generate code, documentation and test cases given an interface file.

For example `./bball/api/openapi.json`:

```json
{
  "openapi" : "3.0.1",
  "info" : {
    "title" : "Basketball REST",
    "description" : "# Basketball Games & Stats API\nScore Sheet and Game Statistics made available with the following frameworks: …",
    "contact" : {
      "name" : "Free Dumb Bytes aka freedumbytes",
      "url" : "https://gitlab.com/freedumbytes/basketball/",
      "email" : "freedumbytes@shadowland.demon.nl"
    },
    "license" : {
      "name" : "Apache License, Version 2.0.",
      "url" : "https://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version" : "1.0.0"
  },
  "externalDocs" : {
    "description" : "OpenAPI 3.0 tutorial.",
    "url" : "https://idratherbewriting.com/learnapidoc/pubapis_openapi_tutorial_overview.html"
  },
  "servers" : [ {
    "url" : "http://freedumbytes.dev.net/tomcat/bball/",
    "description" : "LAN",
    "variables" : { }
  }, {
    "url" : "http://www.shadowland.demon.nl/basketball.xhtml",
    "description" : "WWW",
    "variables" : { }
  } ],
  "tags" : [ {
    "name" : "health",
    "description" : "Application availability."
  } ],
  "paths" : {
    "/api/ping" : {
      "get" : {
        "tags" : [ "health" ],
        "summary" : "Application health.",
        "description" : "Check application availability.",
        "operationId" : "pong",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Application information when it is up and running.",
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/ApplicationInfoIncludeRoot"
                }
              },
              "application/xml" : {
                "schema" : {
                  "$ref" : "#/components/schemas/ApplicationInfo"
                }
              },
              "application/xhtml+xml" : {
                "schema" : {
                  "$ref" : "#/components/schemas/ApplicationInfo"
                }
              }
            }
          },
          "404" : {
            "description" : "Application is down."
          }
        }
      }
    }
  },
  "components" : {
    "schemas" : {
      "ApplicationInfo" : {
        "type" : "object",
        "properties" : {
          "name" : {
            "type" : "string",
            "description" : "Application name.",
            "example" : "Basketball Games & Stats"
          },
          "company" : {
            "type" : "string",
            "description" : "Company name.",
            "example" : "Free Dumb Bytes aka freedumbytes"
          },
          "version" : {
            "type" : "string",
            "description" : "Application version.",
            "example" : "1.0.0"
          },
          "build" : {
            "type" : "string",
            "description" : "Application build date/time.",
            "example" : "2019-02-18 22:40:27 CET"
          },
          "time" : {
            "type" : "string",
            "description" : "Response date/time.",
            "format" : "date-time"
          }
        },
        "description" : "Installed application information.",
        "xml" : {
          "name" : "appInfo"
        }
      },
      "ApplicationInfoIncludeRoot" : {
        "type" : "object",
        "properties" : {
          "appInfo" : {
            "$ref" : "#/components/schemas/ApplicationInfo"
          }
        },
        "description" : "Include root workaround: Installed application information."
      }
    }
  }
}
```

or `./bball/api/openapi.yaml`:


```yaml
openapi: 3.0.1
info:
  title: Basketball REST
  description: |
    # Basketball Games & Stats API
    Score Sheet and Game Statistics made available with the following frameworks:
    * [Jersey](https://jersey.github.io/) RESTful Web Services in Java.
    * [Swagger Core](https://github.com/swagger-api/swagger-core) is a Java implementation of the OpenAPI Specification.
    * [Swagger UI](https://github.com/swagger-api/swagger-ui) to visualize and interact with the APIâ€™s resources without having any of the implementation logic in place.
    * [The OpenAPI Specification](https://swagger.io/specification/) or [OAS](https://github.com/OAI/OpenAPI-Specification/) defines a standard, language-agnostic interface to RESTful APIs.
    * [Swagger Editor](https://github.com/swagger-api/swagger-editor/) lets you [edit](https://editor.swagger.io/) Swagger API specifications in YAML inside your browser and to preview documentations in real time.
    * [Maven Plugin](https://github.com/swagger-api/swagger-codegen/blob/master/modules/swagger-codegen-maven-plugin/README.md) to support the [Swagger Codegen project](https://github.com/swagger-api/swagger-codegen/).
    * [Tools and Integrations](https://swagger.io/tools/open-source/open-source-integrations/) a list of libraries and frameworks serving the Swagger ecosystem.
    * [Single Source of Truth](https://idratherbewriting.com/learnapidoc/pubapis_combine_swagger_and_guide.html): Integrating Swagger UI with the rest of your docs.
  contact:
    name: Free Dumb Bytes aka freedumbytes
    url: https://gitlab.com/freedumbytes/basketball/
    email: freedumbytes@shadowland.demon.nl
  license:
    name: Apache License, Version 2.0.
    url: https://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
externalDocs:
  description: OpenAPI 3.0 tutorial.
  url: https://idratherbewriting.com/learnapidoc/pubapis_openapi_tutorial_overview.html
servers:
- url: http://freedumbytes.dev.net/tomcat/bball/
  description: LAN
  variables: {}
- url: http://www.shadowland.demon.nl/basketball.xhtml
  description: WWW
  variables: {}
tags:
- name: health
  description: Application availability.
paths:
  /api/ping:
    get:
      tags:
      - health
      summary: Application health.
      description: Check application availability.
      operationId: pong
      parameters: []
      responses:
        200:
          description: Application information when it is up and running.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApplicationInfoIncludeRoot'
            application/xml:
              schema:
                $ref: '#/components/schemas/ApplicationInfo'
            application/xhtml+xml:
              schema:
                $ref: '#/components/schemas/ApplicationInfo'
        404:
          description: Application is down.
components:
  schemas:
    ApplicationInfo:
      type: object
      properties:
        name:
          type: string
          description: Application name.
          example: Basketball Games & Stats
        company:
          type: string
          description: Company name.
          example: Free Dumb Bytes aka freedumbytes
        version:
          type: string
          description: Application version.
          example: 1.0.0
        build:
          type: string
          description: Application build date/time.
          example: 2019-02-18 22:40:27 CET
        time:
          type: string
          description: Response date/time.
          format: date-time
      description: Installed application information.
      xml:
        name: appInfo
    ApplicationInfoIncludeRoot:
      type: object
      properties:
        appInfo:
          $ref: '#/components/schemas/ApplicationInfo'
      description: 'Include root workaround: Installed application information.'
```

## Swagger

 * [Swagger Core](https://github.com/swagger-api/swagger-core) is a Java implementation of the OpenAPI Specification.
 * [Swagger Editor](https://github.com/swagger-api/swagger-editor/) lets you [edit](https://editor.swagger.io/) Swagger API specifications in YAML inside your browser and to preview documentations in real time.
 * [Tools and Integrations](https://swagger.io/tools/open-source/open-source-integrations/) a list of libraries and frameworks serving the Swagger ecosystem.
 * [Maven Plugin](https://github.com/swagger-api/swagger-codegen/blob/master/modules/swagger-codegen-maven-plugin/README.md) to support the [Swagger Codegen project](https://github.com/swagger-api/swagger-codegen/).

### Swagger UI

 * [Swagger UI](https://github.com/swagger-api/swagger-ui) to visualize and interact with the API's resources without having any of the implementation logic in place.
 * [Single Source of Truth](https://idratherbewriting.com/learnapidoc/pubapis_combine_swagger_and_guide.html): Integrating Swagger UI with the rest of your docs.

#### Visualization

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-openapi.png "Swagger UI description")

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-schemas-jersey-include-root-workaround.png "Swagger UI schemas")

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-responses-json.png "Swagger UI responses JSON")

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-responses-xml.png "Swagger UI responses XML")

#### Interaction

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-tryout.png "Swagger UI tryout")

![Swagger UI](https://freedumbytes.gitlab.io/basketball/images/swagger-execute.png "Swagger UI execute")
